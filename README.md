# Ek Gösterge Kontrol
Bu programın amacı; İKYS sisteminden indirilen Ek Gösterge Raporu ile elimzideki mevcut bilgilere göre karşılaştırma yaparak, hata olup olmadığını bulmak için hazırlanmıştır. Bu program, İl Müftülüğü Terfi Biriminde çalışan personele kolaylık sağlaması amacıyla hazırlanmıştır.


## Kurulum
Uçbirimi açmak için Masaüstünde iken boş bir yere sağ tık yapın ve Burada Uçbirim Aç'a tıklayın ve aşağıdaki komutları sırasıyla (tek tek - ayrı ayrı) uçbirimden çalıştın. 
```
sudo apt install python3-pip git
sudo python3 -m pip install -U pip pandas openpyxl
git config --global http.sslverify false
git clone https://gitlab.com/yahyayildirim/ek-gosterge-kontrol.git
```
En son komutunu çalıştırdığınızda Masaüstünüzde ek-gosterge-kontrol adında bir klasör oluşmuş olacaktır. 

## Kullanım
DİBBYS --> İKYS --> Raporlar --> Terfi Raporları kısmında EK Gösterge İlerlemesi Cetvelini excel olarak ek-gosterge-kontrol klasörüne indirin, sonra `ikys_ekgosterge_kontrol.py` dosyasına sağ tıklayıp izinlerden çalıştırılabilir tikini işaretleyip kapatın ve dosyaya çift tıklayarak Uçbirimde Çalıştır butonuna tıklayın.... Kontrol raporunuz oluşturulacaktır, gerekli kontrollleri yapabilirsiniz.

## Notlar
Program, istisnaları hesaplayamaz, onun bazı kişilerde kırmızı olarak görülenler hata olmayabilir. Lütfen, bu durumu göz önünde bulundurun.
