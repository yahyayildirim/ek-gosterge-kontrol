#!/usr/bin/env python3

import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import re
import openpyxl
import os.path
import time

def ikys_ekgosterge_verileri():
	print("Lütfen bekleyin....")
	dosya = "EkGostergeIlerlemesiCetveliMaasYapanBurolar.xlsx"
	yeniEkGos = "silme_ekgostergeler.xlsx"
	if os.path.exists(dosya):
		wb = openpyxl.load_workbook(dosya)
		sayfa_sayisi = len(wb.sheetnames)
		sayfa_adi = wb.worksheets[0]
		hucre_degeri = sayfa_adi['A3'].value
		dosya_adi = hucre_degeri.split(':')[-1].strip().replace(' ', '_')

		df_ikys = pd.DataFrame()

		for sayfa in range(sayfa_sayisi):
			sh = wb.worksheets[sayfa]
			c1 = sh['A3'].value
			print(c1.split(':')[-1].strip().replace(' ', '_'))
			dfs = pd.read_excel(dosya, sheet_name=sayfa, skiprows=6)
			dfs['gorev_yeri'] = c1.split(':')[-1].strip().split()[-1]
			df_ikys = df_ikys.append(dfs, ignore_index=True)

		sutun_adi = ['sn', 'sicil', 'sınıf', 'ad-soyad', 'ogrenim', 'unvan', 'ekea-d', 'ekea-k', 'ekea-gos', 'ekea-eski', 'ekea-yeni','kha-d', 'kha-k', 'kha-gos', 'kha-eski', 'kha-yeni','ga-d', 'ga-k', 'ga-gos', 'ga-eski', 'ga-yeni', 'yur_tar', 'gorev_yeri']

		#df_ikys = pd.DataFrame(pd.read_excel(dosya, sheet_name=0, skiprows=7), columns=sutun_adi)
		df_ikys.columns = sutun_adi
		df_ekgos = pd.DataFrame(pd.read_excel(yeniEkGos, sheet_name=0))
		
		for i in df_ikys["ogrenim"].tolist():
			ogrenimbul = re.search("Doçentlik|Master|Doktora|Lisansüstü|Üniversite|Lisans Tamamlama|Yüksek Okul|İmam Hatip|Meslek|Lise|Birinci Devre|Ortaokul|İlköğretim|İlkokul|Okur - Yazar", i)
			df_ikys['ogrenim'] = df_ikys['ogrenim'].replace([i], ogrenimbul.group(0))

		df_ikys['ogrenim'] = df_ikys['ogrenim'].replace(
			['Doçentlik', 'Master', 'Doktora', 'Lisansüstü', 'Üniversite', 'Lisans Tamamlama', 'İmam Hatip', 'Meslek', 'Lise', 'Birinci Devre', 'Ortaokul', 'İlköğretim', 'İlkokul', 'Okur-Yazar'],
			['Yüksek Okul', 'Yüksek Okul', 'Yüksek Okul', 'Yüksek Okul', 'Yüksek Okul', 'Yüksek Okul', 'Diğer', 'Diğer', 'Diğer', 'Diğer', 'Diğer', 'Diğer', 'Diğer', 'Diğer']
			)

		def ekgosterge(unvan, sınıf, ogrenim, derece):
			ekg = df_ekgos.loc[(df_ekgos['unvan'] == unvan) & (df_ekgos['sınıf'] == sınıf) & (df_ekgos['ogrenim'] == ogrenim) & (df_ekgos['derece'] == derece), 'ekgosterge'].sum()
			return ekg

		df_ikys['EKEA_EkGos'] = df_ikys.apply(lambda row: ekgosterge(row["unvan"], row["sınıf"], row["ogrenim"], row["ga-d"]), axis=1)
		df_ikys['KHA_EkGos'] = df_ikys.apply(lambda row: ekgosterge(row["unvan"], row["sınıf"], row["ogrenim"], row["kha-d"]), axis=1)
		df_ikys['GA_EkGos'] = df_ikys.apply(lambda row: ekgosterge(row["unvan"], row["sınıf"], row["ogrenim"], row["ga-d"]), axis=1)

		df_ikys['EKEA_Kontrol'] = df_ikys['EKEA_EkGos']-df_ikys['ekea-yeni']
		df_ikys['KHA_Kontrol'] = df_ikys['KHA_EkGos']-df_ikys['kha-yeni']
		df_ikys['GA_Kontrol'] = df_ikys['GA_EkGos']-df_ikys['ga-yeni']

		df = df_ikys[['sicil', 'sınıf', 'gorev_yeri', 'ad-soyad', 'ogrenim', 'unvan', 'ekea-d', 'ekea-yeni', 'EKEA_EkGos', 'EKEA_Kontrol', 'kha-d', 'kha-yeni', 'KHA_EkGos', 'KHA_Kontrol', 'ga-d', 'ga-yeni', 'GA_EkGos', 'GA_Kontrol']]
		df.sort_values(by=['ad-soyad'], inplace=True)

		def color_negative_red(value):
			if value < 0:
				color = 'white'
				return 'font-weight: bold; background-color: red; color: %s' % color
			elif value > 0:
				color = 'white'
				return 'font-weight: bold; background-color: red; color: %s' % color
			else:
				color = 'black'
				return 'color: %s' % color

		df.style.applymap(color_negative_red, subset=['EKEA_Kontrol','KHA_Kontrol','GA_Kontrol']).to_excel(dosya_adi + "_EkGösterge_Kontrol_Raporu.xlsx", index=False, freeze_panes=(1,5))
		print("Rapor başarılı bir şekilde oluşturulmuştur.")
		time.sleep(2)
	else:
		print("Olmadı dostum, Öncelikle İKYS --> Raporlar --> Terfi Raporları --> Ek Gösterge İlerlemesi Cetveli Maaş Yapan Bürolar linkinden raporu excel olarak bu klasöre indirin sonra işlem yapın.")
		time.sleep(15)

if __name__ == "__main__":
	ikys_ekgosterge_verileri()
